// Create article
USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:///dblp-article.csv" AS row
CREATE (:Article { title: row.title, pages: row.pages, volume: row.volume, number: row.number, url: row.url, ee: row.ee});

USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:///dblp-rl-author.csv" AS row
CREATE (:Author { title: row.title, author: row.author});

// Create incollection
USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:///dblp-incollection.csv" AS row
CREATE (:Incollection {author: row.author, title: row.title, pages: row.pages, year: row.year, volume: row.volume, number: row.number, url: row.url, ee: row.ee});

USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:///dblp-rl-year.csv" AS row
CREATE (:Year { title: row.title, author: row.year});

// Create inproceedings
USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:///dblp-inproceedings.csv" AS row
CREATE (:Inproceedings {author: row.author, title: row.title, pages: row.pages, year: row.year, volume: row.volume, number: row.number, url: row.url, ee: row.ee});


USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:///dblp-rl-journal.csv" AS row
CREATE (:Journal { title: row.title, author: row.journal});

// Create index
CREATE INDEX ON :Article(title);
CREATE INDEX ON :Incollection(title);
CREATE INDEX ON :Inproceedings(title);

CREATE CONSTRAINT ON (a:Article) ASSERT a.title IS UNIQUE;
CREATE CONSTRAINT ON (i:Incollection) ASSERT i.title IS UNIQUE;
CREATE CONSTRAINT ON (i:Inproceedings) ASSERT i.title IS UNIQUE;

schema await

USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:///dblp-rl-author.csv" AS row
MATCH (article:Article {title: row.title})
MATCH (author:Author {name: row.author})

MERGE (article)-[:WROTE_FOR]->(author);

USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:///dblp-rl-year.csv" AS row
MATCH (incollection:Incollection {title: row.title})
MATCH (year:Year {year: row.year})

MERGE (incollection)-[:WROTE_IN]->(year);


USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:///dblp-rl-journal.csv" AS row
MATCH (incollection:incollection {title: row.title})
MATCH (journal:journal {journal: row.journal})

MERGE (incollection)-[:WROTE_IN]->(journal);

USING PERIODIC COMMIT
LOAD CSV WITH HEADERS FROM "file:///dblp-article.csv" AS row
MATCH (article:Article {title: row.title, pages: row.pages, year: row.year, volume: row.volume, number: row.number, url: row.url, ee: row.ee})
MATCH (journal:Journal {name: row.journal})

MERGE (article)-[:WROTE_IN]->(journal);




--------- Command: ---------

bin./neo4j-import --into bdnr.db --id-type string --nodes:Article dblp-article.csv --nodes:Inproceedings dblp-proceedings.csv --nodes:Incollection dblp-incollection.csv  --relationships: HAVE_AUTHORS dblp-rl-author.csv --relationships: HAVE_YEAR dblp-rl-year.csv --relationships: HAVE_JOURNAL dblp-rl-journal.csv