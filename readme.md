# Práctica Base de datos no convencionales

* Lanzar proyecto para convertir las entidades article, inproceddings e incollection a json, cada uno en su ficher
* Importar los 3 json en mongo usando el siguiente comando de mongoImport: ```mongoimport --db [databaseName] --collection [collection] --file dblp_[entidad].json``
mongoimport --db dblp --collection articles --file dblp_article.json`
mongoimport --db dblp --collection inproceedings --file dblp_inproceedings.json
mongoimport --db dblp --collection incollection --file dblp_incollection.json
