import xmltodict, json
from html.parser import HTMLParser

class XmlToJon(object):

    def clean(self,o,collection):

        if '@mdate' in o[collection].keys():
            del (o[collection]['@mdate'])
        if '@key' in o[collection].keys():
            del (o[collection]['@key'])
        if 'author' in o[collection].keys() and isinstance(o[collection]['author'], list):
            for idx,val in enumerate(o[collection]['author']):
                if not isinstance(val, str) and '@orcid' in val.keys():
                    del (val['@orcid'])
                    o[collection]['author'][idx] = str(val['#text'])
        if 'title' in o[collection].keys() and not isinstance(o[collection]['title'], str) and '#text' in o[collection]['title'].keys():
            o[collection]['title'] = str(o[collection]['title']['#text'])

        if 'year' in o[collection].keys() and  isinstance(o[collection]['year'], str):
            o[collection]['year'] = int(o[collection]['year'])


    def process_buffer(self,buf,collection):

        with open("data/dblp_global2.json", "a") as outputfile:
            o = xmltodict.parse(buf)
            self.clean(o,collection)

            o[collection]['type']=collection

            jsonString = str(json.dumps(o))
            outputfile.write(jsonString[len(collection)+4:len(jsonString) ].replace('{{','{').replace('}}','}'))
            outputfile.close()


    def main_process(self):
        htmlparser = HTMLParser()
        inputbuffer = ''

        with open('data/dblp.xml','r', encoding="utf-8") as inputfile:
            inputfile.seek(0)
            append = False
            for line in inputfile:

                if '</article>' in str(line) or '</inproceedings>' in str(line) or '</incollection>' in str(line):
                    finallabel = str(line)
                    inputbuffer += finallabel[:finallabel.find(">")+1]
                    append = False

                    # Saving in json files differents
                    if '</article>' in str(line):
                        self.process_buffer(inputbuffer.strip('\n'),'article')
                    elif '</inproceedings>' in str(line):
                        self.process_buffer(inputbuffer.strip('\n'),'inproceedings')
                    else:
                        self.process_buffer(inputbuffer.strip('\n'),'incollection')

                    inputbuffer = None
                    del inputbuffer #probably redundant...


                if '<article' in str(line) :
                    initialLabel = str(line)
                    inputbuffer = initialLabel[initialLabel.find("<article"):]
                    append = True
                elif '<inproceedings' in str(line) or '<incollection' in str(line):
                    initialLabel = str(line)
                    inputbuffer = initialLabel[initialLabel.find("<in"):]
                    append = True
                elif append:

                    inputbuffer += str(htmlparser.unescape(line.replace('&lt;','menor').replace('&gt','mayor').replace('&','_')))

if __name__ == '__main__':
    xmlToJon = XmlToJon()

    xmlToJon.main_process()
