import xmltodict, json
from html.parser import HTMLParser
import csv


class XmlToCsv(object):

    def __init__(self):
        self.listNodesArticles = []
        self.listNodesInproceedings = []
        self.listNodesIncollections = []
        self.listRLAuthors = []
        self.listRLJournal = []
        self.listRLYear = []

        self.dictAuthors = dict()
        # self.dictJournal = []
        # self.dictYear = []
        self.globalId = 1
        self.authorId=1
        # self.yearId=1
        # self.journalId=1


    def clean(self,o,collection):

        if '@mdate' in o[collection].keys():
            del (o[collection]['@mdate'])
        if '@key' in o[collection].keys():
            del (o[collection]['@key'])
        if 'author' in o[collection].keys() and isinstance(o[collection]['author'], list):
            for idx,val in enumerate(o[collection]['author']):
                if not isinstance(val, str) and '@orcid' in val.keys():
                    del (val['@orcid'])
                    o[collection]['author'][idx] = str(val['#text'])
        elif 'author' in o[collection].keys() and not isinstance(o[collection]['author'], str) and '@orcid' in o[collection]['author'].keys():
            del (o[collection]['author']['@orcid'])
            o[collection]['author'] = str(o[collection]['author']['#text'])
        if 'title' in o[collection].keys() and not isinstance(o[collection]['title'], str):
            o[collection]['title'] = str(o[collection]['title']['#text'])

        if 'year' in o[collection].keys() and  isinstance(o[collection]['year'], str):
            o[collection]['year'] = int(o[collection]['year'])


    def process_buffer(self,buf,collection):
        resultado = xmltodict.parse(buf)
        self.clean(resultado, collection)
        print('---'+collection)
        resultado[collection]['title'] = resultado[collection]['title'].replace('G\\""','_').replace('"',"_").replace(',',"-")
        resultado[collection]['id'] = self.globalId



        if collection == 'article':
            self.listNodesArticles.append(resultado)
        elif collection == 'inproceedings':
            self.listNodesInproceedings.append(resultado)
        elif collection == 'incollection':
            self.listNodesIncollections.append(resultado)

        if collection == 'inproceedings' :
            if resultado[collection].get('author'):
                if isinstance(resultado[collection]['author'], list):
                    for author in resultado[collection]['author']:
                        val = {}
                        val_author = {}

                        val['id'] = self.globalId
                        val['author'] = self.authorId

                        val_author['id'] = self.authorId
                        val_author['name'] = author

                        if not author in  self.dictAuthors:
                            self.dictAuthors[author] = self.authorId
                            val['author'] = self.authorId
                        else:
                            val['author'] = self.dictAuthors[author]

                        self.listRLAuthors.append(val)

                        self.authorId = self.authorId + 1
                else:
                    val = {}
                    val_author = {}

                    val['id'] = self.globalId

                    val_author['id'] = self.authorId
                    val_author['name'] = resultado[collection]['author']



                    if not resultado[collection]['author'] in self.dictAuthors:
                        self.dictAuthors[resultado[collection]['author']] = self.authorId
                        val['author'] = self.authorId
                    else:
                        val['author'] = self.dictAuthors[resultado[collection]['author']]

                    self.listRLAuthors.append(val)
                    self.authorId = self.authorId + 1


            # if resultado[collection].get('journal'):
            #     val = {}
            #     val_journal = {}
            #
            #     val['id'] = self.globalId
            #     val['journal'] = self.journalId
            #
            #     val_journal['id'] = self.journalId
            #     val_journal['name'] = resultado[collection]['journal']
            #
            #     self.listRLJournal.append(val)
            #     self.listJournal.append(val_journal)
            #
            #     self.journalId = self.journalId + 1
            #
            # if resultado[collection].get('year'):
            #     val = {}
            #     val_year = {}
            #
            #     val['id'] = self.globalId
            #     val['year'] = self.yearId
            #
            #     val_year['id'] = self.yearId
            #     val_year['name'] = resultado[collection]['year']
            #
            #     self.listRLYear.append(val)
            #     self.listYear.append(val_year)
            #
            #     self.yearId = self.yearId + 1

        self.globalId = self.globalId + 1



    def write_file_article(self):

        print('Writing article')

        firstTime = True
        with open("data/dblp-article.csv", 'w', newline='') as csvfile:
            writer = csv.writer(csvfile)
            headers = [ 'id:ID(Article)','title', 'pages', 'volume', 'number', 'url', 'ee']
            for element in self.listNodesArticles:
                article = element.get('article')
                if article.get('title') is not '.':

                    if firstTime:

                        firstTime = False
                        writer.writerow(headers)
                        print("--------------------- HEADERS ARTICLE ---------------------")
                        print()
                        print(headers)
                        print()

                        headers = ['id','title', 'pages', 'volume', 'number', 'url', 'ee']

                    targetrow = []
                    for key in headers:
                        if article.get(key):
                            targetrow.append(article[key])
                        else:
                            targetrow.append('')
                    writer.writerow(targetrow)


    def write_file_inproceedings(self):
        print("Writing inproceedings")

        firstTime = True
        with open("data/dblp-inproceedings.csv", 'w', newline='') as csvfile:
            writer = csv.writer(csvfile)
            headers = [ 'id:ID(Inproceedings)','title', 'pages',  'crossref', 'booktitle', 'url', 'ee']
            for element in self.listNodesInproceedings:
                inproceedings = element.get('inproceedings')

                if firstTime:


                    firstTime = False
                    writer.writerow(headers)
                    print("--------------------- HEADERS INPROCEEDINGS ---------------------")
                    print()
                    print(headers)
                    print()
                    headers = ['id','title', 'pages', 'crossref', 'booktitle', 'url', 'ee']

                targetrow = []
                for key in headers:
                    if inproceedings.get(key):
                        targetrow.append(inproceedings[key])
                    else:
                        targetrow.append('')
                writer.writerow(targetrow)


    def write_file_incollection(self):
        print("Writing incollection")

        firstTime = True
        with open("data/dblp-incollection.csv", 'w', newline='') as csvfile:
            writer = csv.writer(csvfile)
            headers = ['id:ID(Incollection)','title', 'pages',  'crossref', 'booktitle', 'url', 'ee']
            for element in self.listNodesIncollections:
                incollection = element.get('incollection')

                if firstTime:


                    firstTime = False
                    writer.writerow(headers)
                    print("--------------------- HEADERS INCOLLECTION ---------------------")
                    print()
                    print(headers)
                    print()
                    headers = ['id','title', 'pages', 'crossref', 'booktitle', 'url', 'ee']

                targetrow = []
                for key in headers:
                    if incollection.get(key):
                        targetrow.append(incollection[key])
                    else:
                        targetrow.append('')
                writer.writerow(targetrow)

    def write_file_author(self):
        print("Writing Author")
        firstTime = True
        with open("data/dblp-author.csv", 'w', newline='') as csvfile:
            writer = csv.writer(csvfile)
            headers = ['id:ID(Author)','name']
            for key in self.dictAuthors:

                if firstTime:
                    
                    firstTime = False
                    writer.writerow(headers)
                    print("--------------------- HEADERS AUTHORS ---------------------")
                    print()
                    print(headers)
                    print()
                    headers = ['id','name']

                targetrow = []

                targetrow.append(self.dictAuthors[key])
                targetrow.append(key)
                writer.writerow(targetrow)

    def write_rl_file_author(self):
        print("Writing RL Author")
        firstTime = True
        with open("data/dblp-rl-author.csv", 'w', newline='') as csvfile:
            writer = csv.writer(csvfile)
            headers = [':START_ID(Inproceedings)', ':END_ID(Author)']
            for element in self.listRLAuthors:
                if element.get('title') is not '.':
                    if firstTime:


                        firstTime = False
                        writer.writerow(headers)
                        print("--------------------- HEADERS RL_AUTHORS ---------------------")
                        print()
                        print(headers)
                        print()
                        headers = ['id', 'author']

                    targetrow = []
                    for key in headers:
                        if element.get(key):
                            targetrow.append(element[key])
                        else:
                            targetrow.append('')
                    writer.writerow(targetrow)

    #  def write_file_journal(self):
    #     print("Writing journal")
    #     firstTime = True
    #     with open("data/dblp-journal.csv", 'w', newline='') as csvfile:
    #         writer = csv.writer(csvfile)
    #         headers = ['id:ID(Journal)','name']
    #         for element in list(self.listJournal):
    #
    #             if firstTime:
    #
    #
    #                 firstTime = False
    #                 writer.writerow(headers)
    #                 print("--------------------- HEADERS JOURNALS ---------------------")
    #                 print()
    #                 print(headers)
    #                 print()
    #                 headers = ['id', 'name']
    #
    #             targetrow = []
    #             for key in headers:
    #                 if element.get(key):
    #                     targetrow.append(element[key])
    #                 else:
    #                     targetrow.append('')
    #             writer.writerow(targetrow)
    #
    #
    # def write_rl_file_journal(self):
    #     print("Writing RL Journal")
    #     firstTime = True
    #     with open("data/dblp-rl-journal.csv", 'w', newline='') as csvfile:
    #         writer = csv.writer(csvfile)
    #         headers = [':START_ID(Inproceedings)', ':END_ID(Journal)']
    #         for element in self.listRLJournal:
    #             if element.get('title') is not '.':
    #                 if firstTime:
    #
    #
    #                     firstTime = False
    #                     writer.writerow(headers)
    #                     print("--------------------- HEADERS RL_JOURNALS ---------------------")
    #                     print()
    #                     print(headers)
    #                     print()
    #                     headers = ['id', 'journal']
    #
    #                 targetrow = []
    #                 for key in headers:
    #                     if element.get(key):
    #                         targetrow.append(element[key])
    #                     else:
    #                         targetrow.append('')
    #                 writer.writerow(targetrow)
    #
    # def write_file_year(self):
    #     print("Writing Year")
    #     firstTime = True
    #     with open("data/dblp-year.csv", 'w', newline='') as csvfile:
    #         writer = csv.writer(csvfile)
    #         headers = ['id:ID(Year)','name']
    #         for element in list(self.listYear):
    #
    #             if firstTime:
    #
    #
    #                 firstTime = False
    #                 writer.writerow(headers)
    #                 print("--------------------- HEADERS YEAR ---------------------")
    #                 print()
    #                 print(headers)
    #                 print()
    #                 headers = ['id', 'name']
    #
    #             targetrow = []
    #             for key in headers:
    #                 if element.get(key):
    #                     targetrow.append(element[key])
    #                 else:
    #                     targetrow.append('')
    #             writer.writerow(targetrow)
    #
    # def write_rl_file_year(self):
    #     print("Writing RL Year")
    #     firstTime = True
    #     with open("data/dblp-rl-year.csv", 'w', newline='') as csvfile:
    #         writer = csv.writer(csvfile)
    #         headers = [':START_ID(Inproceedings)', ':END_ID(Year)']
    #         for element in self.listRLYear:
    #             if element.get('title') is not '.':
    #                 if firstTime:
    #
    #
    #                     firstTime = False
    #                     writer.writerow(headers)
    #                     print("--------------------- HEADERS RL_YEARS ---------------------")
    #                     print()
    #                     print(headers)
    #                     print()
    #                     headers = ['id', 'year']
    #
    #                 targetrow = []
    #                 for key in headers:
    #                     if element.get(key):
    #                         targetrow.append(element[key])
    #                     else:
    #                         targetrow.append('')
    #                 writer.writerow(targetrow)


    def main_process(self):
        htmlparser = HTMLParser()
        inputbuffer = ''

        with open('data/dblp.xml','r', encoding="utf-8") as inputfile:
            inputfile.seek(0)
            append = False
            for line in inputfile:

                if '</article>' in str(line) or '</inproceedings>' in str(line) or '</incollection>' in str(line):
                    finallabel = str(line)
                    inputbuffer += finallabel[:finallabel.find(">")+1]
                    append = False

                    # Saving in json files differents
                    if '</article>' in str(line):
                        self.process_buffer(inputbuffer.strip('\n'),'article')
                    elif '</inproceedings>' in str(line):
                        self.process_buffer(inputbuffer.strip('\n'),'inproceedings')
                    else:
                        self.process_buffer(inputbuffer.strip('\n'),'incollection')

                    inputbuffer = None
                    del inputbuffer #probably redundant...


                if '<article' in str(line) :
                    initialLabel = str(line)
                    inputbuffer = initialLabel[initialLabel.find("<article"):]
                    append = True
                elif '<inproceedings' in str(line) or '<incollection' in str(line):
                    initialLabel = str(line)
                    inputbuffer = initialLabel[initialLabel.find("<in"):]
                    append = True
                elif append:

                    inputbuffer += str(htmlparser.unescape(line.replace('&lt;','menor').replace('&gt','mayor').replace('&','_')))

if __name__ == '__main__':
    xmlToCsv = XmlToCsv()

    xmlToCsv.main_process()
    xmlToCsv.write_file_article()
    xmlToCsv.write_file_incollection()
    xmlToCsv.write_file_inproceedings()
    xmlToCsv.write_file_author()
    # xmlToCsv.write_file_journal()
    # xmlToCsv.write_file_year()
    xmlToCsv.write_rl_file_author()
    # xmlToCsv.write_rl_file_journal()
    # xmlToCsv.write_rl_file_year()
